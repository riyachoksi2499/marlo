/*
Title : Controller
Purpose: Charterer Controller
Created On : 1 Dec 2022
*/

import 'package:flutter/material.dart';
import 'package:marlo/Networks/api_endpoint.dart';
import 'package:marlo/Networks/api_response.dart';
import 'package:marlo/common/global.dart';
import 'package:marlo/screens/model/charterer_model.dart';

class ChartererService {
  //Get Charterer list
  static Future getSearchApi(String chartererName, {context}) async {
    String url = urlBase + urlSearch + chartererName;
    List<SearchData> searchValues = <SearchData>[];
    dynamic arrData;
    await getresponse(url, context).then((value) => {
          if (value != null && value[kStatusCode] as String == 'SUCCESS')
            {
              arrData = value[kData]
                  .map<SearchData>((json) => SearchData.fromJson(json))
                  .toList(),
              searchValues = arrData,
            }
          else
            {
              searchValues.clear(),
              snackBar(context, value[kMessage].toString()),
            }
        });

    return searchValues;
  }

  //Add Charterer
  static Future addChartererApi(
      String name, email, address, state, city, country, website, number,
      {context}) async {
    String url = urlBase + urlAddName;
    if (context != null) {
      Map<String, dynamic> map = {
        "name": name,
        "email": email,
        "address1": address,
        "address2": "",
        "state": state,
        "city": city,
        "country": country,
        "website": website,
        "contactPerson": "XYZ",
        "phoneNumber": number
      };

      Map<String, dynamic> data = {"chartererDetails": map};

      postDataRequest(url, data, context).then((value) => {
            if (value.statusCode == 200 || value.statusCode == 201)
              {
                snackBar(context, 'Record Added successfully'),
                Navigator.pop(context),
              }
            else
              {
                snackBar(context, 'Record not added'),
              }
          });
    }
  }
}
