/*
Title : Add Record
Purpose: Add Charterer
Created On : 29 Nov 2022
*/
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:marlo/common/app_colors.dart';
import 'package:marlo/common/app_strings.dart';
import 'package:marlo/screens/bloc/charterer_bloc.dart';
import 'package:marlo/screens/view/search_screen.dart';
import 'package:marlo/utility/commom_text_field.dart';
import 'package:marlo/utility/texts.dart';

class AddRecordScreen extends StatefulWidget {
  const AddRecordScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<AddRecordScreen> createState() => _AddRecordScreenState();
}

class _AddRecordScreenState extends State<AddRecordScreen> {
  //Controllers
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController websiteController = TextEditingController();
  //Form key
  final _formKey = GlobalKey<FormState>();
  ChartererBLoC chartererBLoC = ChartererBLoC();

  //checking validations before adding charterer
  void _submit() {
    final isValid = _formKey.currentState!.validate();
    if (!isValid) {
      return;
    } else {
      chartererBLoC = ChartererBLoC(
          name1: nameController.text,
          email1: emailController.text,
          address1: addressController.text,
          state1: stateController.text,
          city1: cityController.text,
          country1: countryController.text,
          website1: websiteController.text.contains('http')
              ? websiteController.text
              : 'https://${websiteController.text}',
          number1: mobileController.text,
          cntx: context);
    }
    _formKey.currentState!.save();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Image.asset(
            'assets/images/arrow.png',
            color: MediaQuery.of(context).platformBrightness == Brightness.dark
                ? AppColors.whiteColor
                : AppColors.blackColor,
            scale: 1.5,
          ),
        ),
        backgroundColor:
            MediaQuery.of(context).platformBrightness == Brightness.light
                ? AppColors.whiteColor
                : AppColors.blackColor,
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 16.w, right: 16.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 22.85.h, bottom: 32.h),
              //Add Charterer
              child: setHeeboText(
                AppStrings.addCharterer,
                25.sp,
                MediaQuery.of(context).platformBrightness == Brightness.dark
                    ? AppColors.whiteColor
                    : AppColors.blackColor,
                FontWeight.w700,
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //Name
                      commonTextFormField(nameController, AppStrings.name,
                          TextInputAction.next, TextInputType.name, (value) {
                        if (value!.isEmpty) {
                          return 'Enter name';
                        }
                        return null;
                      }, context),
                      //Email
                      commonTextFormField(
                          emailController,
                          AppStrings.email,
                          TextInputAction.next,
                          TextInputType.emailAddress, (value) {
                        if (value!.isEmpty ||
                            !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(value)) {
                          return 'Enter a valid email!';
                        }
                        return null;
                      }, context),
                      //Country
                      commonTextFormField(countryController, AppStrings.country,
                          TextInputAction.next, TextInputType.name, (value) {
                        if (value!.isEmpty) {
                          return 'Enter country';
                        }
                        return null;
                      }, context),
                      //Country code
                      _buildCountryPickerDropdown(),
                      //Address
                      commonTextFormField(
                          addressController,
                          AppStrings.address,
                          TextInputAction.next,
                          TextInputType.streetAddress, (value) {
                        if (value!.isEmpty) {
                          return 'Enter address';
                        }
                        return null;
                      }, context),
                      //State
                      commonTextFormField(stateController, AppStrings.state,
                          TextInputAction.next, TextInputType.name, (value) {
                        if (value!.isEmpty) {
                          return 'Enter state';
                        }
                        return null;
                      }, context),
                      //City
                      commonTextFormField(cityController, AppStrings.city,
                          TextInputAction.next, TextInputType.name, (value) {
                        if (value!.isEmpty) {
                          return 'Enter city';
                        }
                        return null;
                      }, context),
                      //Website
                      commonTextFormField(websiteController, AppStrings.website,
                          TextInputAction.done, TextInputType.text, (value) {
                        if (value!.isEmpty) {
                          return 'Enter website';
                        }
                        return null;
                      }, context),
                      SizedBox(
                        height: 10.h,
                      ),
                      //click here
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const SearchScreen()));
                        },
                        child: RichText(
                            text: TextSpan(children: <TextSpan>[
                          TextSpan(
                              text: AppStrings.searchAgain,
                              style: TextStyle(
                                  fontFamily: 'NotoSans',
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.greyColor)),
                          TextSpan(
                              text: AppStrings.clickHere,
                              style: TextStyle(
                                  fontFamily: 'NotoSans',
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.blueColor)),
                        ])),
                      )
                    ],
                  ),
                ),
              ),
            ),
            //Continue
            InkWell(
              onTap: () {
                _submit();
              },
              child: Container(
                margin: EdgeInsets.only(bottom: 8.h),
                width: MediaQuery.of(context).size.width,
                height: 48.h,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: AppColors.buttonBackgroundColor),
                alignment: Alignment.center,
                child: const Text(
                  'Continue',
                  style: TextStyle(color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  //Country Dropdown
  _buildCountryPickerDropdown(
      {bool filtered = false,
      bool sortedByIsoCode = false,
      bool hasPriorityList = false,
      bool hasSelectedItemBuilder = false}) {
    double dropdownButtonWidth = MediaQuery.of(context).size.width / 3;
    double dropdownItemWidth = dropdownButtonWidth - 30;
    double dropdownSelectedItemWidth = dropdownButtonWidth - 30;
    return Row(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          width: 95.w,
          margin: EdgeInsets.only(bottom: 8.h),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.h),
              color:
                  MediaQuery.of(context).platformBrightness == Brightness.dark
                      ? AppColors.listViewBackDarkModeColor
                      : AppColors.textFieldBackColor),
          child: CountryPickerDropdown(
            icon: const SizedBox(),
            isDense: false,
            selectedItemBuilder: hasSelectedItemBuilder == true
                ? (Country country) => _buildDropdownSelectedItemBuilder(
                    country, dropdownSelectedItemWidth)
                : null,
            itemBuilder: (Country country) => hasSelectedItemBuilder == true
                ? _buildDropdownItemWithLongText(country, dropdownItemWidth)
                : _buildDropdownItem(country, dropdownItemWidth),
            initialValue: 'AR',
            itemFilter: filtered
                ? (c) => ['AR', 'DE', 'GB', 'CN'].contains(c.isoCode)
                : null,
            priorityList: hasPriorityList
                ? [
                    CountryPickerUtils.getCountryByIsoCode('GB'),
                    CountryPickerUtils.getCountryByIsoCode('CN'),
                  ]
                : null,
            sortComparator: sortedByIsoCode
                ? (Country a, Country b) => a.isoCode.compareTo(b.isoCode)
                : null,
            onValuePicked: (Country country) {},
          ),
        ),
        SizedBox(
          width: 8.w,
        ),
        Expanded(
          child: commonTextFormField(mobileController, AppStrings.mobile,
              TextInputAction.done, TextInputType.text, (value) {
            if (value!.isEmpty) {
              return 'Enter phone no';
            }
            return null;
          }, context),
        )
      ],
    );
  }

  Widget _buildDropdownItem(Country country, double dropdownItemWidth) =>
      SizedBox(
        width: dropdownItemWidth,
        child: Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CountryPickerUtils.getDefaultFlagImage(country),
              SizedBox(
                width: 2.w,
              ),
              Expanded(child: Text("+${country.phoneCode}")),
            ],
          ),
        ),
      );

  Widget _buildDropdownItemWithLongText(
          Country country, double dropdownItemWidth) =>
      SizedBox(
        width: dropdownItemWidth,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              CountryPickerUtils.getDefaultFlagImage(country),
              SizedBox(
                width: 8.w,
              ),
              Expanded(child: Text(country.name)),
            ],
          ),
        ),
      );

  Widget _buildDropdownSelectedItemBuilder(
          Country country, double dropdownItemWidth) =>
      SizedBox(
          width: dropdownItemWidth,
          child: Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                children: <Widget>[
                  CountryPickerUtils.getDefaultFlagImage(country),
                  SizedBox(
                    width: 2.w,
                  ),
                  Expanded(
                      child: Text(
                    country.name,
                    style: TextStyle(
                        fontSize: 14.sp,
                        color: Colors.red,
                        fontWeight: FontWeight.bold),
                  )),
                ],
              )));
}
