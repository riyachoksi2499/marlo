/*
Title : Search Record
Purpose: Search Charterer
Created On : 29 Nov 2022
*/

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:marlo/screens/bloc/charterer_bloc.dart';
import 'package:marlo/screens/model/charterer_model.dart';
import 'package:marlo/common/app_colors.dart';
import 'package:marlo/common/app_strings.dart';
import 'package:marlo/screens/view/add_record_screen.dart';
import 'package:marlo/utility/commom_text_field.dart';
import 'package:marlo/utility/texts.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  TextEditingController searchController = TextEditingController();
  ChartererBLoC chartererBLoC = ChartererBLoC();
  List<SearchData> searchValues = [];

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor:
          MediaQuery.of(context).platformBrightness == Brightness.light
              ? AppColors.transparentColor
              : AppColors.blackColor, // navigation bar color
      statusBarColor:
          MediaQuery.of(context).platformBrightness == Brightness.light
              ? AppColors.transparentColor
              : AppColors.blackColor, // status bar color
      statusBarIconBrightness:
          MediaQuery.of(context).platformBrightness == Brightness.dark
              ? Brightness.light
              : Brightness.dark, // status bar icons' color
      systemNavigationBarIconBrightness:
          MediaQuery.of(context).platformBrightness == Brightness.dark
              ? Brightness.light
              : Brightness.dark, //navigation bar icons' color
    ));
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Scaffold(
            body: Stack(
          children: [
            Container(
              height: height,
              width: width,
              color:
                  MediaQuery.of(context).platformBrightness == Brightness.light
                      ? AppColors.backColor
                      : AppColors.blackColor,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: height - 50.h,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(24.r),
                      topRight: Radius.circular(24.r)),
                  color: MediaQuery.of(context).platformBrightness ==
                          Brightness.light
                      ? AppColors.whitef7
                      : AppColors.blackColor,
                ),
                child: SingleChildScrollView(child: searchWidget()),
              ),
            ),
          ],
        )));
  }

  //Search Listing
  Widget searchWidget() {
    return Padding(
      padding: EdgeInsets.only(left: 16.w, right: 16.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Padding(
              padding: EdgeInsets.only(top: 20.h, bottom: 12.h),
              child: Container(
                width: 50.w,
                height: 5.h,
                decoration: BoxDecoration(
                    color: AppColors.transBlue,
                    borderRadius: BorderRadius.all(Radius.circular(2.5.r))),
              ),
            ),
          ),
          setNotoText(
              AppStrings.charterer,
              16.sp,
              MediaQuery.of(context).platformBrightness == Brightness.dark
                  ? AppColors.whiteColor
                  : AppColors.blackColor,
              FontWeight.w600),
          SizedBox(height: 11.h),
          SizedBox(
            height: 32.h,
            child: searchTextField(
                searchController,
                AppStrings.search,
                TextInputAction.done,
                TextInputType.text,
                const Icon(
                  Icons.search,
                  color: AppColors.greyColor,
                ),
                12.sp, (v) {
              setState(() {
                if (v.isNotEmpty) {
                  chartererBLoC = ChartererBLoC(val: v, cntx: context);
                } else {
                  searchValues.clear();
                }
              });
            }, context),
          ),
          //Search List
          Padding(
            padding: EdgeInsets.only(top: 20.h, bottom: 20.h),
            child: Container(
              height: 392.h,
              decoration: BoxDecoration(
                  color: MediaQuery.of(context).platformBrightness ==
                          Brightness.light
                      ? AppColors.whiteColor
                      : AppColors.listViewBackDarkModeColor,
                  borderRadius: BorderRadius.all(Radius.circular(10.r))),
              child: StreamBuilder(
                  stream: chartererBLoC.searchChartererList,
                  builder: (context, AsyncSnapshot snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                      case ConnectionState.waiting:
                      case ConnectionState.active:
                      case ConnectionState.done:
                        if (snapshot.hasError) {
                          return Text('There was an error : ${snapshot.error}');
                        } else if (snapshot.hasData) {
                          searchValues = snapshot.data as List<SearchData>;
                        }

                        return searchValues.isEmpty
                            ? Container()
                            : ListView.builder(
                                primary: false,
                                shrinkWrap: true,
                                padding: EdgeInsets.zero,
                                itemCount: searchValues.length,
                                itemBuilder: (context, index) {
                                  return Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                          padding: EdgeInsets.only(
                                              top: 20.h, left: 16.w),
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                32.w,
                                            child: setNotoText(
                                                searchValues[index]
                                                    .chartererName
                                                    .toString(),
                                                14.sp,
                                                MediaQuery.of(context)
                                                            .platformBrightness ==
                                                        Brightness.dark
                                                    ? AppColors.whiteColor
                                                    : AppColors.blackColor,
                                                FontWeight.w400,
                                                max: 2),
                                          ))
                                    ],
                                  );
                                },
                              );
                    }
                  }),
            ),
          ),

          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const AddRecordScreen()));
            },
            child: RichText(
                text: TextSpan(children: <TextSpan>[
              TextSpan(
                  text: AppStrings.find,
                  style: TextStyle(
                      fontFamily: 'NotoSans',
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w400,
                      color: AppColors.greyColor)),
              TextSpan(
                  text: AppStrings.addNow,
                  style: TextStyle(
                      fontFamily: 'NotoSans',
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w400,
                      color: AppColors.blueColor)),
            ])),
          )
        ],
      ),
    );
  }
}
