/*
Title : Bloc
Purpose: Charterer Bloc
Created On : 1 Dec 2022
*/
import 'dart:async';
import 'package:marlo/screens/controller/charterer_controller.dart';

class ChartererBLoC {
  dynamic chartererName,
      name,
      email,
      address,
      state,
      city,
      country,
      website,
      number;
  dynamic context;

  Stream get searchChartererList async* {
    if (context != null) {
      yield await ChartererService.getSearchApi(chartererName,
          context: context);
    }
  }

  Stream get addCharterer async* {
    if (context != null) {
      yield await ChartererService.addChartererApi(
          name, email, address, state, city, country, website, number,
          context: context);
    }
  }

  final StreamController _searchCharterer = StreamController();
  final StreamController _addCharterer = StreamController();

  ChartererBLoC(
      {val,
      name1,
      email1,
      address1,
      state1,
      city1,
      country1,
      website1,
      number1,
      cntx}) {
    if (cntx != null) {
      context = cntx;
      if (val != null) {
        chartererName = val;
        searchChartererList.listen((v) => _searchCharterer.add(v));
      } else {
        name = name1;
        email = email1;
        address = address1;
        state = state1;
        city = city1;
        country = country1;
        website = website1;
        number = number1;
        addCharterer.listen((v) => _addCharterer.add(v));
      }
    }
  }
}
