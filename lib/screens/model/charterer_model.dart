/*
Title : Charterer Model Class
Purpose: Serach Charterer
Created On : 29 Nov 2022
*/

class SearchModel {
  List<SearchData>? data;
  String? errorFlag;
  String? message;

  SearchModel({this.data, this.errorFlag, this.message});

  SearchModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <SearchData>[];
      json['data'].forEach((v) {
        data!.add(SearchData.fromJson(v));
      });
    }
    errorFlag = json['error_flag'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['error_flag'] = errorFlag;
    data['message'] = message;
    return data;
  }
}

class SearchData {
  String? chartererId;
  String? chartererName;
  String? tier;

  SearchData({this.chartererId, this.chartererName, this.tier});

  SearchData.fromJson(Map<String, dynamic> json) {
    chartererId = json['chartererId'];
    chartererName = json['chartererName'];
    tier = json['Tier'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['chartererId'] = chartererId;
    data['chartererName'] = chartererName;
    data['Tier'] = tier;
    return data;
  }
}
