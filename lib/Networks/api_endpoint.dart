/*
Title : Api endpoints
Purpose: All api endpoints
Created On : 29 Nov 2022
*/

const urlBase = 'https://asia-southeast1-marlo-bank-dev.cloudfunctions.net/api_dev/';

const urlSearch = "contracts/charterers/search?charterer_name=";
const urlAddName = "contracts/charterers";
