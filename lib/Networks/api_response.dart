import 'package:dio/dio.dart';
import 'package:marlo/common/app_strings.dart';
import 'package:marlo/common/global.dart';

Future<dynamic> getresponse(String url, context) async {
  try {
    var header = Options(
      validateStatus: (_) => true,
      headers: {"authToken": AppStrings.authToken},
    );

    Response response = await Dio().get(url, options: header);
    if (response.statusCode == 200) {
      return response.data;
    } else {
      snackBar(context, 'Request failed with status: ${response.statusCode}.');
      return response;
    }
  } catch (e) {
    print(e);
  }
}

Future<dynamic> postDataRequest(String urlPath, params, context) async {
  try {
    var header = Options(
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/x-www-form-urlencoded',
        "authToken": AppStrings.authToken
      },
    );
    Response<String> response =
        await Dio().post(urlPath, data: params, options: header);
    if (response.statusCode == 200 || response.statusCode == 201) {
      return response;
    } else {
      snackBar(context, 'Request failed with status: ${response.statusCode}.');
      return response;
    }
  } catch (e) {
    return e.toString();
  }
}
