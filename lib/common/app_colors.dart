/*
Title : AppColors
Purpose: All AppColors
Created On : 29 Nov 2022
*/

import 'package:flutter/material.dart';

class AppColors {
  static const Color transparentColor = Colors.transparent;
  static const Color transBlue = Color(0xffC6EBF6);
  static const Color whiteColor = Colors.white;
  static const Color whitef7 = Color(0xffF7F7F7);
  static const Color blackColor = Colors.black;
  static const Color greyColor = Color(0xff75808A);
  static const Color blueColor = Color(0xff0CABDF);
  static const Color listViewBackDarkModeColor = Color(0xff232323);
  static const Color containerBackgroundColor = Color(0xffE9EEF0);
  static const Color buttonBackgroundColor = Color(0xff0CABDF);
  static const Color textFieldBackColor = Color(0xffE9EEF0);
  static const Color backColor = Color.fromRGBO(0, 0, 0, 0.6);
}
