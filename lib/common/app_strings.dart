/*
Title : AppStrings
Purpose: All AppStirngs
Created On : 29 Nov 2022
*/

class AppStrings {
  static const String authToken =
      "eyJhbGciOiJSUzI1NiIsImtpZCI6ImE5NmFkY2U5OTk5YmJmNWNkMzBmMjlmNDljZDM3ZjRjNWU2NDI3NDAiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbWFybG8tYmFuay1kZXYiLCJhdWQiOiJtYXJsby1iYW5rLWRldiIsImF1dGhfdGltZSI6MTY2OTg4NDE2NCwidXNlcl9pZCI6IlJoSGdiY1U0cHZNMGR3RE90d1piTlhPOTlRMjMiLCJzdWIiOiJSaEhnYmNVNHB2TTBkd0RPdHdaYk5YTzk5UTIzIiwiaWF0IjoxNjY5ODg0MTY0LCJleHAiOjE2Njk4ODc3NjQsImVtYWlsIjoieGlob2g1NTQ5NkBkaW5lcm9hLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJ4aWhvaDU1NDk2QGRpbmVyb2EuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.tgcoX6G5CpqT5DqV5gPOiNCeP5dNlks0o_Pk8ncCLzIbo_vwmu5ZO5ONFhEwlIxPqeSB7WqqqkQIheayM4-2MSxRUY4tyWRx3of3LxMu34SncLZFEXweyNDBtjVTPUsaJSa2G7hUoGzctfQdLcxwrmLK_SqfSC5G1wK6dCSyTbbzBolcaS7VSuvUpmnTd8j1FU-L9oDVY07gd1-ZL1w8vyRtItutlmbMnNso1rZLJ5w9qse7DjcaurLnWyQIKGBsN723Oc6z2CEUxTDJ8xiph4BUz-YwXjSaC77DGt9tNguks87Ap2vuMnvn2APNBHart3Iw1qU7eepwZCXxDJcJ3A";
  static const String charterer = 'Charterer';
  static const String search = 'Search';
  static const String find = 'Can’t find your Charterer? ';
  static const String addNow = 'Add now';
  static const String addCharterer = 'Add charterer';
  static const String name = 'Full Name';
  static const String email = 'Email';
  static const String country = 'Country of residence';
  static const String mobile = 'Mobile number';
  static const String address = 'Address';
  static const String state = 'State';
  static const String city = 'City';
  static const String website = 'Website';
  static const String searchAgain = 'Want to search again?';
  static const String clickHere = 'Click Here';
}
