/*
Title : Global
Purpose: All Global Variables
Created On : 29 Nov 2022
*/

import 'package:flutter/material.dart';

const kData = "data";
const kStatusCode = "error_flag";
const kMessage = "message";

String capitalize(val) {
  return val[0].toString().toUpperCase() + val.substring(1);
}

snackBar(BuildContext context, String message) {
  return ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      behavior: SnackBarBehavior.floating,
      content: Text(message),
      duration: const Duration(seconds: 2),
    ),
  );
}
