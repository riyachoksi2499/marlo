import 'package:flutter/material.dart';
import 'package:marlo/common/global.dart';

setNotoText(String text, double size, Color color, FontWeight weight, {max}) {
  return Text(
    text.isNotEmpty ? capitalize(text) : '',
    maxLines: max,
    style: TextStyle(
      fontSize: size,
      fontWeight: weight,
      fontFamily: 'NotoSans',
      color: color,
    ),
  );
}

setHeeboText(String text, double size, Color color, FontWeight weight, {max}) {
  return Text(
    text.isNotEmpty ? capitalize(text) : '',
    maxLines: max,
    style: TextStyle(
      fontSize: size,
      fontWeight: weight,
      fontFamily: 'Heebo',
      color: color,
    ),
  );
}
