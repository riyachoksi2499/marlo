import 'package:flutter/material.dart';
import 'package:marlo/common/app_colors.dart';

class MyThemes{
  static final darkTheme=ThemeData(
    scaffoldBackgroundColor: AppColors.blackColor,
    colorScheme: const ColorScheme.dark()
  );
  static final lightTheme=ThemeData(
    scaffoldBackgroundColor: AppColors.whiteColor,
    colorScheme: const ColorScheme.light()
  );
}