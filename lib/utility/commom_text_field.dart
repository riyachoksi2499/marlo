import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:marlo/common/app_colors.dart';

commonTextFormField(
    final TextEditingController? controller,
    final String? hintText,
    final TextInputAction? textInputAction,
    final TextInputType? keyboardType,
    final FormFieldValidator<String> validator,
    BuildContext? context) {
  return Container(
    margin: EdgeInsets.only(bottom: 8.h),
    child: TextFormField(
      textInputAction: textInputAction,
      controller: controller,
      style: TextStyle(
        fontSize: 14.sp,
        fontWeight: FontWeight.w400,
        fontFamily: 'Heebo',
        color: MediaQuery.of(context!).platformBrightness == Brightness.dark
            ? AppColors.whiteColor
            : AppColors.blackColor,
      ),
      cursorColor: Colors.black,
      keyboardType: keyboardType,
      validator: validator,
      decoration: InputDecoration(
        counterText: "",
        hintText: hintText,
        hintStyle: TextStyle(
            fontSize: 14.sp,
            fontWeight: FontWeight.w400,
            fontFamily: 'Heebo',
            color: AppColors.greyColor),
        filled: true,
        fillColor: MediaQuery.of(context).platformBrightness == Brightness.dark
            ? AppColors.listViewBackDarkModeColor
            : AppColors.textFieldBackColor,
        contentPadding: EdgeInsets.only(top: 20.r, left: 12.r),
        border: OutlineInputBorder(
            borderSide: BorderSide(
                color:
                    MediaQuery.of(context).platformBrightness == Brightness.dark
                        ? AppColors.listViewBackDarkModeColor
                        : AppColors.textFieldBackColor),
            borderRadius: BorderRadius.all(Radius.circular(10.r))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color:
                    MediaQuery.of(context).platformBrightness == Brightness.dark
                        ? AppColors.listViewBackDarkModeColor
                        : AppColors.textFieldBackColor),
            borderRadius: BorderRadius.all(Radius.circular(10.r))),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color:
                    MediaQuery.of(context).platformBrightness == Brightness.dark
                        ? AppColors.listViewBackDarkModeColor
                        : AppColors.textFieldBackColor),
            borderRadius: BorderRadius.all(Radius.circular(10.r))),
      ),
    ),
  );
}

searchTextField(
    TextEditingController? controller,
    String? hintText,
    TextInputAction? textInputAction,
    TextInputType? keyboardType,
    Widget? suffixIcon,
    double size,
    void Function(String)? onChange,
    BuildContext? context) {
  return SizedBox(
    child: TextFormField(
      controller: controller,
      onChanged: onChange,
      textInputAction: textInputAction,
      style: TextStyle(
          fontSize: size,
          fontWeight: FontWeight.w400,
          fontFamily: 'NotoSans',
          color: MediaQuery.of(context!).platformBrightness == Brightness.dark
              ? AppColors.whiteColor
              : AppColors.blackColor),
      cursorColor: Colors.black,
      keyboardType: keyboardType,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(top: 13.r, left: 12.r),
        suffixIcon: suffixIcon,
        hintText: hintText,
        hintStyle: TextStyle(
            fontSize: size,
            fontWeight: FontWeight.w400,
            fontFamily: 'NotoSans',
            color: AppColors.greyColor),
        filled: true,
        fillColor: MediaQuery.of(context).platformBrightness == Brightness.dark
            ? AppColors.listViewBackDarkModeColor
            : AppColors.textFieldBackColor,
        border: OutlineInputBorder(
            borderSide: BorderSide(
                color:
                    MediaQuery.of(context).platformBrightness == Brightness.dark
                        ? AppColors.listViewBackDarkModeColor
                        : AppColors.textFieldBackColor),
            borderRadius: BorderRadius.all(Radius.circular(10.r))),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color:
                    MediaQuery.of(context).platformBrightness == Brightness.dark
                        ? AppColors.listViewBackDarkModeColor
                        : AppColors.textFieldBackColor),
            borderRadius: BorderRadius.all(Radius.circular(10.r))),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
                color:
                    MediaQuery.of(context).platformBrightness == Brightness.dark
                        ? AppColors.listViewBackDarkModeColor
                        : AppColors.textFieldBackColor),
            borderRadius: BorderRadius.all(Radius.circular(10.r))),
      ),
    ),
  );
}
